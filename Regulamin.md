## 1 **Celem organizacji Kulturalne Rewolucje dalej zwany jako KR** 
Utrzymywanie przestrzeni opartej na swobodzie wyrażaniu myśli i poglądów w cywilizowany i podlegający regulaminowi KR sposób .

### 1.1 Procedura zmiany procesu/regulaminu/statusu dalej zwany PRS

##### 1.1.1 tryb/prządek procedowania 
1.1.1.1 ogłoszenie posiedzenia musi mieć miejsce co najmniej 5 dni wcześniej<br>
1.1.1.2 liderzy frakcji i kół posiadają zdolność do decydowania za przyjęciem wniosku wg "standardowego procentu składowego"<br>
1.1.1.3 obowiązek tworzenia notatki ze spotkania podczas każdorazowej ingerencji w prs jest obowiązkiem prowadzącego spotkanie, lub jego zastępcy<br>
1.1.1.3.1 struktura notatki musi zawierać: <br>
- imię i nazwisko prowadzącego
- powód organizacji spotkania
- lista numerów wniosków - tytuł wniosku; wynik posiedzenia<br>
1.1.1.4 zmiana PRS może trwać do miesiąca czasu i dopiero po publicznym obwieszczeniu i udostępnieniu zmian zostaje wcielona w życie i obowiązuje<br>
##### 1.1.2 regularność procedowania 
1.1.2.1 obrady dot zmian PRS odbywają się w każdą sobotę o godzinie 22:00 . obrady mogą trwać maksymalnie do dwóch godzin<br>
1.1.2.2 w przypadku braku czasu na procedowanie wszystkich wniosków , należy pozostałe przełożyć na kolejne posiedzenie<br>
##### 1.1.3 format wniosków
- imię i nazwisko - członek frakcji lub koła
- tytuł, tytuły lub opis
- zmodyfikowany PRS lub usunięcie elementu 
- uzasadnienie 
##### 1.1.4 zatwierdzenie formatu 
1.1.4.1 po zatwierdzeniu poprawności formatu, wniosek zostaje przyjęty do procedowania ogłoszony co najmniej tydzień wcześniej<br>
1.1.4.2 zatwierdzony wniosek musi znaleźć się w publicznym repozytorium<br>
## 2 Frakcje
*kultura i jakość*
##### 2.1 prawa
3. 2.1 możliwość składania wniosków zmian PRS 
3. 2.2 *możliwość brania udziału w debatach*
3. 2.3 możliwość wnioskowania na pomysły do debat
3. 2.4 60% udziałów głosowań
##### 2.2 obowiązki
- przynależność do przynajmniej jednego koła

##### 2.3 wzór wniosku nowej frakcji
- Nazwa Frakcji
- Imię i Nazwisko - imię lidera
- Motto 

## 3 Koła
Koła są kluczowym tworem wewnątrz KR pozwalającym skutecznie rozstrzygać o zmianach w regulaminie wprowadzając profesjonalizm do debat i innych programów
#### 3.1 prawa
- możliwość składania wniosków zmian PRS
- *możliwość brania udziału w debatach*
- możliwość zgłaszania pomysłów do debat
- 20% udziału w głosowaniach

#### 3.2 obowiązki
2.2.1 określony profil i sferę zainteresowania np: matematyka, hip-hop

#### 3.3 wzór wniosku nowego koła
- Nazwa Koła
- imię i nazwisko lidera
- Typ koła: Eksperci/Artyści
- Profil: np. matematyka, historia, biznes
- Motto

## 4 **Resorty**
#### 4.1 *Human Resources* 
##### 4.1.1 cele: organizowanie posiedzeń zarządu i organizowanie debat
##### 4.1.2 obowiązki: 
- rejestrowania posiedzeń zarządu i debat
- przyjmowanie wniosków na oficjalnym kanale discord 'wnioski'
##### 4.1.3 skład członkowski: Justyn Damaz
##### 4.1.4 powoływanie
o zatwierdzeniu nominacji decyduje *Project Management* 
##### 4.1.5 prawa 
4.2.5.1 10% udziałów głosowań<br>
4.1.2.2 organizowanie spotkań<br>
4.1.2.3 organizowanie organizowanie głosowania
	
#### 4.2 *Project Management*
##### 4.2.1 cele
4.2.2.1 przyjmowanie i zatwierdzanie nominacji na resorty wewnętrzne
##### 4.2.2 obowiązki 
- przyjmowania nowych nominacji
- podejmowania decyzji dotyczących mianowania członków na poszczególne resorty 
##### 4.2.3 skład członkowski: Justyn Damaz
##### 4.2.4 prawa 
- 10% udziałów głosowań
- możliwość uruchomienia trybu nadzwyczajnego

## 5 Nadzwyczajne
W przypadku zmiany lub uchylania poszczególnych punktów PRS podczas obrad jednorazową uchwałą zatwierdzoną przez co najmniej 90% zgromadzonych uprawnionych do zatwierdzania wniosków członków KR, należy utworzyć notatkę opisującą potrzebę zaistnienia jednorazowej uchwały.
#### 5.1 *Obrady*
- ogłoszenie musi mieć miejsce co najmniej 48 godziny wcześniej
- odczytanie z notatki powód spotkania, następnie
- odczytanie tytułów numerów wniosków wraz z treścią lakonicznie
- prowadzący zarządza głosowanie
- w przypadku uzyskania jednomyślności 82%
- wnioski zostają zatwierdzone
#### 5.1 *Głosowanie*
- ogłoszenie musi mieć miejsce co najmniej 48 godziny wcześniej zawierające tematy do dyskursu
- podlega legislacji w przypadku wyniku zebrania 82%
- notatka z głosowania zawiera informację o wyniku głosowania



*** 

*KR* - kulturalne rewolucje 
*PRS* - proces/regulamin/status




