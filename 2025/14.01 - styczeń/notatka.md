Justyn Damaz, Klepacze, rapsy, Kultura i Sztuka
**nadzwyczajne spotkanie zarządu**

w wyniku prawa do ogłoszenia spotkania 24 godziny wcześniej
w wyniku normalizacji zagadnień spotkań, obrad i głosowania
w wyniku zainteresowania

nr4_01.25 - **prawo do zwołania spotkania nadzwyczajnego**
nr3_01.25 - **obowiązek tworzenia notatki z posiedzenia**
nr6_01.25 - **udzielenie resortom udziału w głosowaniu**
nr1_01.25 - **wdrożenie struktur i procesów kół organizacji**
nr4_12.24 - **wdrożenie struktur frakcji organizacji**

we wszystkich wnioskach uzyskano konsensus 82%

ponadto:
- zaktualizowano kolejność wybranych zapisów regulaminu w celu utrzymania porządku
- obowiązek przynależności do przynajmniej jednego koła podlega frakcji, a więc i jego zapis
- zapisy przyjmowania wniosków podlegają HR dlatego znalazły się w dziale HR



