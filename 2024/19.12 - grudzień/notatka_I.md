1. Zaistniała potrzeba aktualizacji numerów regulaminu w czasie rzeczywistym w celach porządkowych bez zmian zawartości ich treści . Zmiany dotyczyły procedur składania wniosków zmian PRS . 

2. Zaktualizowano lub dodano tytuły poszczególnych punktów regulaminu wyłącznie w celach porządkowych

3. Wnioski nr_1, nr_2, nr_3 zostały dostosowane do odpowiedniego formatu i przyjęte do procedowania bez ogłoszenia ich wcześniej zgodnie z przyjętymi w regulaminie okresie


