Justyn Damaz, Klepacze 
**Zapisywanie wyników głosowania.**

2.2.2 Zapisywanie informacji o tym czy wniosek został zatwierdzony lub odrzucony 

Umożliwienie obiegu dokumentów, transparentność.
___

**Tworzenie notatek ze spotkań dotyczących zmiany/uchylania poszczególnych punktów regulaminu zatwierdzoną po przez 80% większości zgromadzonych i uprawnionych do zatwierdzania wniosków członków KR.** 

2.5 W przypadku zmiany lub uchylania poszczególnych punktów PRS podczas obrad jednorazową uchwałą zatwierdzoną przez co najmniej 90% zgromadzonych uprawnionych do zatwierdzania wniosków członków KR, należy utworzyć notatkę opisującą potrzebę zaistnienia jednorazowej uchwały.

Celem jest dynamiczny rozwoju KR wraz z utrzymaniem wysokiego poziomu transparentności 
